import sys
sys.path.append("..")

import re
import pandas

from common import core


class FileLoader(core.Core):
    def __init__(self, working_dir="", distribution_file="generator.in", run_file="photo_track.in", log_dir=None, log_file_name=None, console_log=False, logger_name=None) -> None:
        super().__init__(working_dir, distribution_file,
                         run_file, log_dir, log_file_name, console_log,
                         logger_name=logger_name)

    def get_Xemit_file(self, run_number):
        return self.working_dir.joinpath(f"{self.run_file.stem}.Xemit.{str(run_number).zfill(3)}")

    def get_Yemit_file(self, run_number):
        return self.working_dir.joinpath(f"{self.run_file.stem}.Yemit.{str(run_number).zfill(3)}")

    def get_Zemit_file(self, run_number):
        return self.working_dir.joinpath(f"{self.run_file.stem}.Zemit.{str(run_number).zfill(3)}")

    def get_TRemit_file(self, run_number):
        return self.working_dir.joinpath(f"{self.run_file.stem}.TRemit.{str(run_number).zfill(3)}")

    def output_files_exists(self, run_number, Xemit=True, Yemit=True, Zemit=True, TRemit=False):
        """Method to check if the output files (Xemit, Yemit, Zemit, TRemit) exist.

        Args:
            Xemit (bool, optional): If True, existance of Xemit will be checked. Defaults to True.
            Xemit (bool, optional): If True, existance of Yemit will be checked. Defaults to True.
            Zemit (bool, optional): If True, existance of Zemit will be checked. Defaults to True.
            TRemit (bool, optional): If True, existance of TRemit will be checked. Defaults to False.
        """
        
        if Xemit:
            if not self.get_Xemit_file(run_number).is_file():
                return False
        if Yemit:
            if not self.get_Yemit_file(run_number).is_file():
                return False
        if Zemit:
            if not self.get_Zemit_file(run_number).is_file():
                return False
        if TRemit:
            if not self.get_TRemit_file(run_number).is_file():
                return False
            
        return True

    def get_zpos_file(self, run_number, zpos):
        # if in word is set to end, find last zpos file
        if str(zpos).lower().strip() == "end":
            zpos_list = [int(f.name.split(".")[1]) for f in self.working_dir.iterdir() if f.is_file(
            ) and re.match(fr".*{self.run_file.stem}\.[0-9][0-9][0-9][0-9]\.{str(run_number).zfill(3)}", str(f))]
            file_name = f"{self.run_file.stem}.{str(max(zpos_list)).zfill(4)}.{str(run_number).zfill(3)}"
        # else return name of the zpos file on given zpos
        else:
            file_name = f"{self.run_file.stem}.{str(zpos).zfill(4)}.{str(run_number).zfill(3)}"
        return self.working_dir.joinpath(file_name)

    def get_Xemit(self, run):
        return pandas.read_csv(self.get_Xemit_file(run), delimiter="\s+", names=["z", "t", "x_avr", "x_rms", "xBar_rms", "epsilon", "xxBar"])

    def get_Yemit(self, run):
        return pandas.read_csv(self.get_Yemit_file(run), delimiter="\s+", names=["z", "t", "y_avr", "y_rms", "yBar_rms", "epsilon", "yyBar"])

    def get_Zemit(self, run):
        return pandas.read_csv(self.get_Zemit_file(run), delimiter="\s+", names=["z", "t", "E", "z_rms", "E_rms", "epsilon", "zEBar"])

    def get_TRemit(self, run):
        return pandas.read_csv(self.get_TRemit_file(run), delimiter="\s+", names=["z", "t", "epsilonX", "epsilonY", "epsilonZ"])

    def get_at_zpos(self, run_number, zpos):
        return pandas.read_csv(self.get_zpos_file(run_number, zpos), delimiter="\s+", names=["x", "y", "z", "px", "py", "pz", "t", "charge", "index", "flag"])

    def get_unit(self, parameter):
        # bunch parameters units
        units = {
            "energy_spread": "KeV",
            "x_emit": "mm mrad",
            "y_emit": "mm mrad",
            "x_rms": "mm",
            "y_rms": "mm",
            "Active particle ratio": "/",
            "Beam size (z_rms)": "mm",
        }
        try:
            # gun parameters units
            unit = None
            unit = "deg" if "Phi" in parameter else unit
            unit = "T" if "MaxB" in parameter else unit
            unit = "mm" if "sig" in parameter else unit
            unit = "nC" if "Q_total" in parameter else unit
            unit = "mm" if "Lx" in parameter else unit
            unit = "ns" if "Lt" in parameter else unit
            unit = "MeV" if "MaxE" in parameter else unit
            
            return unit if unit is not None else units[parameter]
        except:
            return parameter

        