import sys 
sys.path.append("..")

import pandas as pd
import pathlib
from config import configuration
import filelock

class OutputFileHandler(object):
    def __init__(self, working_dir:str, output_file_name="run_output.xlsx") -> None:
        """Class for handling the output file, reading and writing to it.

        Args:
            working_dir (str | pathlib.Path): Directory, where the output file should be written.
            output_file_name (str, optional): Name of the output file. Defaults to "run_output.xlsx".
        """
        # Load main config file and set data directory
        self.config = configuration.load_config()
        self.data_dir = pathlib.Path(self.config["Paths"]["data_dir"])
        
        self.working_dir = self.data_dir.joinpath(working_dir)
        self.output_file = self.working_dir.joinpath(output_file_name)
        # obtain lock for the output file
        self.output_file_lock = filelock.FileLock(self.output_file.parent / (self.output_file.name + ".lock"))
        
        self.output_content = self.read_output_file()
        
    @property
    def output_content(self) -> pd.DataFrame:
        """Content of the output file.

        Returns:
            pd.DataFrame: Content of the output file in pd.DataFrame format.
        """
        if self.output_file.exists():
            file_data = self.read_output_file()
            self.output_content = pd.concat([self._output_content, file_data]).drop_duplicates().reset_index(drop=True)
        return self._output_content
    
    @output_content.setter
    def output_content(self, value):
        self._output_content = value
        
    @property
    def working_dir(self) -> pathlib.Path:
        return self._working_dir
    
    @working_dir.setter
    def working_dir(self, working_dir:str) -> None:
        # make sure it is a pathlib instance
        self._working_dir = pathlib.Path(working_dir)
        
    def read_output_file(self) -> pd.DataFrame:
        """Method to read the content of the output file.

        Returns:
            pd.DataFrame: Content of the output file.
        """
        # if the file does not exist, return empty dataframe
        if not self.output_file.exists():
            return pd.DataFrame({"Run number": []})
        # make sure it loads correctly either from excel or csv
        if self.output_file.suffix == ".xlsx":
            return pd.read_excel(self.output_file, index_col=0)
        else:
            return pd.read_csv(self.output_file, index_col=0)
        
    def write_output_file(self) -> None:
        """Method to write updated content to the output file.
        """
        with self.output_file_lock:
            if self.output_file.exists():
                file_data = self.read_output_file()
                self.output_content = pd.concat([self.output_content, file_data]).drop_duplicates().reset_index(drop=True)
                self.output_file.unlink()
            self.output_content = self.output_content.sort_values("Run number")
            if self.output_file.suffix == ".xlsx":
                self.output_content.to_excel(self.output_file)
            else:
                self.output_content.to_csv(self.output_file)
        
    def add_output(self, to_append:dict) -> None:
        """Method add output to the output file.

        Args:
            to_append (dict | pd.DataFrame): Data to append.
        """
        self.output_content = pd.concat([self.output_content, pd.DataFrame(to_append)])
        self.write_output_file()