import sys
sys.path.append("..")

import re
import argparse
import filelock

class ChangeSettings(object):
    def __init__(self, file_name: str) -> None:
        """Class for reading and changing settings in ASTRA run and distribution files

        Args:
            file_name (str): Name of the file with settings.
        """
        self.file = file_name
        # TODO: check if exists
        self.file_lock = filelock.FileLock(self.file.parent / (self.file.name + ".lock"))

        self.file_content = self.load_current_settings()

    def load_current_settings(self) -> str:
        """Function to load the current settings from given file

        Returns:
            str: Content of the file (settings)-
        """
        output = ""
        with self.file_lock:
            with open(self.file, "r") as f:
                output = f.read()
        return output

    def find_setting(self, parameter: str) -> list:
        """Searches for the position of the parameter in given file.

        Args:
            parameter (str): name of parameter to be searched for

        Returns:
            list : list pythocontaining start and end index of parameter including its value in the input file
        """
        # Setting can be separated by a new line or by comma -> two approaches and the shorter one is chosen

        # If searched for the keyword, brackets are not allowed -> escape character has to be used
        search_parameter = parameter.replace("(", "\(").replace(")", "\)")

        # add space to the beginning for correct re search (and word separation, see the [\W])
        file_content = " "+self.file_content
        # Search for both options
        # [\W] search for non alphanumeric character for correct word separation
        new_line_separated = re.search(
            f"[\W]{search_parameter}\s*=.+?\n", file_content)
        comma_separated = re.search(
            f"[\W]{search_parameter}\s*=.+?,", file_content)

        if new_line_separated is None and comma_separated is None:
            return None

        # If only one is valid (is not None)
        if new_line_separated is None and comma_separated is not None:
            # - 1 to account for the one whitespace added to the begining
            # only at the end, as the the beginning is tackled by the [\W] in re.search
            return [comma_separated.start(), comma_separated.end()-1]
        if new_line_separated is not None and comma_separated is None:
            # - 1 to remove the non aplhanumeric character added by [\W]
            # only at the end, as the the beginning is tackled by the [\W] in re.search
            return [new_line_separated.start(), new_line_separated.end()-1]

        # Both are valid -> the shortes is picked
        if new_line_separated.end() - new_line_separated.start() > comma_separated.end() - comma_separated.start():
            # - 1 to remove the non aplhanumeric character added by [\W]
            # only at the end, as the the beginning is tackled by the [\W] in re.search
            return [comma_separated.start(), comma_separated.end()-1]
        else:
            # - 1 to remove the non aplhanumeric character added by [\W]
            # only at the end, as the the beginning is tackled by the [\W] in re.search
            return [new_line_separated.start(), new_line_separated.end()-1]

    def replace_setting(self, parameter: str, new_value: str) -> None:
        """Replaces the value of given parameter by new_value

        Args:
            parameter (str): parameter to be adjusted
            new_value (str): new value of the parameter
        """
        # Get position of the setting
        setting_position = self.find_setting(parameter)

        # If setting not found
        if setting_position is None:
            print(self.file_content)
            print("Parameter not found!")
            exit(1)

        # If found, replace it by the new setting
        self.file_content = self.file_content.replace(
            self.file_content[setting_position[0]: setting_position[1]-1], f"{parameter}={new_value}")

    def read_setting(self, parameter: str) -> str:
        """Looks the given parameter and returns the value of this parameter fro the input file.

        Args:
            parameter (str): parameter to search for

        Returns:
            str: value of given parameter
        """
        # Get position of the setting
        setting_position = self.find_setting(parameter)

        # If setting not found
        if setting_position is None:
            print(self.file_content)
            print("Parameter not found!")
            exit(1)

        # Get the parameter + value without the last character (comma / new line)
        output = self.file_content[setting_position[0]: setting_position[1]-1]
        # Return output after = sign
        return output[re.search("=", output).end():].strip()

    def save_updated_settings(self) -> None:
        """Saves updated settings.
        """
        with self.file_lock:
            with open(self.file, 'w') as f:
                f.write(self.file_content)

    def update(self, parameter, new_value):
        """Sets parameter to n_value

        Args:
            parameter (str): parameter to be changed
            new_value (str): new value of the parameter
        """
        self.replace_setting(parameter, new_value)
        self.save_updated_settings()


def main():
    parser = argparse.ArgumentParser(
        description='Class for adjusting input file parametres.')

    parser.add_argument('--file', type=str, action='store',
                        help='Input file to be adjusted.')
    parser.add_argument('--parameter', type=str, action='store',
                        help='Name of parameter to be adjusted.')
    parser.add_argument('--new_value', type=str, action='store',
                        help='New value of the adjusted parameter.')
    parsed = parser.parse_args()

    c = ChangeSettings(parsed.file)
    print(c.read_setting(parsed.parameter))
    c.update(parsed.parameter, parsed.new_value)
    print(c.read_setting(parsed.parameter))


if __name__ == '__main__':
    main()
