import sys
from traceback import print_tb
sys.path.append("..")

import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
import re

import dash_core_components as dcc
import dash_html_components as html
import dash

from plot_analysis import plot_core

class Plot3DPhaseSpace(plot_core.PlotCore):
    def __init__(self, run_number, working_dir="", distribution_file="generator.in", run_file="photo_track.in", log_dir="log", log_file_name="plots_log.log", console_log=True, plots_dir="plots", logger_name=None) -> None:
        super().__init__(working_dir, distribution_file, run_file, log_dir, log_file_name, console_log, plots_dir, logger_name)
        self.run_number = run_number
        # get the data
        self.data = self.get_data_along_trajectory()
        
    def get_data_along_trajectory(self) -> pd.DataFrame:
        # get all the trajectory markers
        self.trajectory_markers = [int(f.name.split(".")[1]) for f in self.working_dir.iterdir() if f.is_file(
            ) and re.match(fr".*{self.run_file.stem}\.[0-9][0-9][0-9][0-9]\.{str(self.run_number).zfill(3)}", str(f))]
        # set default displaying to the end of trajectory
        self.display_at_zpos = max(self.trajectory_markers)
        # save number of markers for later use
        self.n_markers = len(self.trajectory_markers)

        def make_absolute(df:pd.DataFrame) -> pd.DataFrame:
            # make values of first (reference) particle relative -> set to zero
            df.loc[0, "x"],df.loc[0, "y"],df.loc[0, "z"] = 0,0,0
            df.loc[0, "px"],df.loc[0, "py"],df.loc[0, "pz"] = 0,0,0
            df.loc[0, "t"] = 0
            
            # to mm
            df["x"] *= 1000
            df["y"] *= 1000
            df["z"] *= 1000
            
            df["t"] *= 1000 # to ps
            return df
        
        # load data from every marker and append them to one dataframe
        dfs = []
        for marker in self.trajectory_markers:
            df_to_append = self.get_at_zpos(self.run_number, marker)
            df_to_append["zpos"] = marker
            df_to_append = make_absolute(df_to_append)   
            dfs += [df_to_append]
            
        return pd.concat(dfs)
    
    @property
    def histogram_limits(self):
        if not hasattr(self, "_histogram_limits"):
            self._histogram_limits = {}
            for p in ["x", "y", "z", "t"]:
                self._histogram_limits[p] = [self.data[p].min(), self.data[p].max()]
        return self._histogram_limits
    
    def get_3D_plot(self, zpos:int) -> go.Figure:
        # get the data on the give zpos
        df = self.data.loc[self.data["zpos"] == zpos]
        
        # this was meant to show the longitudial axis, but does not work
        # TODO: ddecide if this will be fixed or removed
        axis = pd.DataFrame(dict(
            x = [0,0],
            y = [0,0],
            z = [self.data["z"].max(), self.data["z"].max()]
        ))
        
        scatter = px.scatter_3d(df, x='x', y='y', z='z',
                           labels={
                            "x": "x [mm]",
                            "y": "y [mm]",
                            "z": "z [mm]",
                            }, title="3D Visualisation (scatter)",
                           range_x=self.histogram_limits["x"],
                           range_y=self.histogram_limits["y"],
                           range_z=self.histogram_limits["z"]
                           )
        line = px.line_3d(axis, x='x', y='y', z='z')
        
        # combine line and scatter to one image
        fig = go.Figure(data=scatter.data + line.data, 
                         frames=[
                            go.Frame(data=fr1.data + fr2.data, name=fr1.name)
                            for fr1, fr2 in zip(line.frames, scatter.frames)
                        ],
                        layout=scatter.layout)
        fig.update_traces(line=dict(color="red",width=10))
        
        return fig
    
    def get_x_histogram(self, zpos:int) -> px.histogram:
        df = self.data.loc[self.data["zpos"] == zpos]
        
        fig = px.histogram(df, x="x", histnorm='probability density',
                           labels={
                                "x": "x [mm]",
                            }, title="Particle density (x axis)", range_x=self.histogram_limits["x"]
                           )
        return fig
    
    def get_y_histogram(self, zpos:int) -> px.histogram:
        df = self.data.loc[self.data["zpos"] == zpos]
        
        fig = px.histogram(df, x="y", histnorm='probability density',
                           labels={
                             "y": "y [mm]",
                            }, title="Particle density (y axis)", range_x=self.histogram_limits["y"]
                           )
        return fig
    
    def get_charge_density_t(self, zpos:int) -> px.histogram:
        df = self.data.loc[self.data["zpos"] == zpos]
        
        fig = px.histogram(df, x="t", histnorm='density',
                           labels={
                             "t": "t [ps]",
                            }, title="Particle density (time projection)", range_x=self.histogram_limits["t"]
                           )
        return fig
    
    def get_charge_density_z(self, zpos:int) -> px.histogram:
        df = self.data.loc[self.data["zpos"] == zpos]
        
        fig = px.histogram(df, x="z", histnorm='density',
                           labels={
                             "z": "z [mm]",
                            }, title="Particle density (z axis)", range_x=self.histogram_limits["z"]
                           )
        return fig
        
    def plot(self) -> None:
        fig = self.get_3D_plot(100)
        
        fig.show()
        
    def get_head(self) -> list:
        ls = [
            html.H1("ASTRA | Phase Space Visualisation Tool"),
        ]
        return ls
        
    def create_dash_layout(self) -> list:
        layout = []
        layout = layout + self.get_head()

        layout = layout + [
            html.Div([
                html.Div(id='left_graphs_section', children=[
                        dcc.Graph(id="graph_3Dplot", figure=self.get_3D_plot(100), style={'width': '110vh', 'height': '80vh'}, )
                        ], className="six columns", style={'width': '55%'}),
                html.Div(id="right_graph_section", children=[
                    html.Div([
                        html.Div([
                            dcc.Graph(id="graph_Xdensity", figure=self.get_x_histogram(100), style={'width': '40vh', 'height': '35vh'}, ),
                        ], className="six columns"),
                        html.Div([
                            dcc.Graph(id="graph_Ydensity", figure=self.get_y_histogram(100), style={'width': '40vh', 'height': '35vh'}, ),
                        ], className="six columns")
                    ], className="row"),
                    html.Div([
                        html.Div([
                            dcc.Graph(id="graph_chargeDensity_t", figure=self.get_charge_density_t(100), style={'width': '40vh', 'height': '35vh'}, ),
                        ], className="six columns"),
                        html.Div([
                            dcc.Graph(id="graph_chargeDensity_z", figure=self.get_charge_density_z(100), style={'width': '40vh', 'height': '35vh'}, ),
                        ], className="six columns"),
                    ], className="row"),    
                ], className="six columns", style={'width': '40%'}),
            ], className="row"),
            html.Div([
                html.Div([
                    html.Button(id="minus_button", n_clicks=0, children="-"),
                    html.Button(id="plus_button", n_clicks=0, children="+"),
                ], className="six columns", style={'width': '15%'}),
                html.Div([
                    dcc.Slider(
                        id='my-LED-display-slider-1',
                        min=min(self.trajectory_markers),
                        max=max(self.trajectory_markers),
                        step=100,
                        value=self.display_at_zpos,
                        marks={
                            marker: f"{marker} cm" for marker in self.trajectory_markers
                        }
                    ),
                ], className="six columns", style={'width': '80%'}),
            ], className="row"),
        ]

        return layout
    
    def start_dash_app(self) -> None:
        print("Starting dash.")
        external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
        app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
        app.layout = html.Div(self.create_dash_layout())
        self.callbacks(app)
        app.run_server(debug=True)
        
    def callbacks(self, app:dash.Dash) -> None:
        @app.callback(
        [dash.Output('graph_3Dplot', 'figure'),
         dash.Output('graph_Xdensity', 'figure'),
         dash.Output('graph_Ydensity', 'figure'),
         dash.Output('graph_chargeDensity_t', 'figure'),
         dash.Output('graph_chargeDensity_z', 'figure')
        ],
        dash.Input('my-LED-display-slider-1', 'value')
        )
        def update_output(value):
            self.display_at_zpos = value
            return self.get_3D_plot(value), self.get_x_histogram(value), self.get_y_histogram(value), self.get_charge_density_t(value), self.get_charge_density_z(value)

        @app.callback(
        dash.Output('my-LED-display-slider-1', 'value'),
        dash.Input('minus_button', 'n_clicks'),
        dash.Input('plus_button', 'n_clicks')
        )
        def update_output(minus_click, plus_click):
            ctx = dash.callback_context
            trigger_id = ctx.triggered[0]["prop_id"].split(".")[0]
            if trigger_id == "minus_button":
                if self.display_at_zpos != min(self.trajectory_markers):
                    self.display_at_zpos-=100
            else:
                if self.display_at_zpos != max(self.trajectory_markers):
                    self.display_at_zpos+=100 
            return self.display_at_zpos
def main():
    plotter = Plot3DPhaseSpace("14", working_dir="UNIFORM_v2\Final")
    plotter.start_dash_app()
    
if __name__ == '__main__':
    main()