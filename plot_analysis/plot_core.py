import sys
sys.path.append("..")

import re
import pandas

from common import file_loader


class PlotCore(file_loader.FileLoader):
    def __init__(self, working_dir="", distribution_file="generator.in", run_file="photo_track.in", log_dir="log", log_file_name="plots_log.log", console_log=True, plots_dir="plots", logger_name=None, load_output_file=True) -> None:
        super().__init__(working_dir, distribution_file,
                         run_file, log_dir, log_file_name, console_log,
                         logger_name=logger_name)

        self.default_colors= ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
                                '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
                                '#bcbd22', '#17becf']

        if load_output_file:
            self.output_handler = self.get_output_file_handler(working_dir)
            self.output = self.output_handler.output_content

        self.plots_dir = self.working_dir.joinpath(plots_dir)
        self.plots_dir.mkdir(parents=True, exist_ok=True)
        self.logger.info(f"Plots directory set to: {self.plots_dir} ")

    def load_run_output_file(self):
        df = pandas.read_csv(self.run_output_file, header=0, index_col=False)
        return df

        