import sys
sys.path.append("..")

import pandas as pd
import re
import numpy as np
import matplotlib.pyplot as plt

from plot_analysis import plot_phase_space

class PlotEmittance(plot_phase_space.PlotPhaseSpace):
    def __init__(self, run_number:int, working_dir="", distribution_file="generator.in", run_file="photo_track.in", log_dir="log", log_file_name="plots_log.log", console_log=True, plots_dir="plots") -> None:
        super().__init__(run_number, "end", working_dir, distribution_file, run_file, log_dir, log_file_name, console_log, plots_dir)
    
        # create directory for emittance plots
        self.plots_dir = self.plots_dir.parents[0].joinpath(f"RUN_{run_number}_emittance")
        self.plots_dir.mkdir(exist_ok=True, parents=True)
        self.logger.info(f"Plot will be saved to: \n{self.plots_dir}")
        
        # get list of z points, where bunch properties are logged
        self.zpos_list = [int(f.name.split(".")[1]) for f in self.working_dir.iterdir() if f.is_file(
        ) and re.match(fr".*{self.run_file.stem}\.[0-9][0-9][0-9][0-9]\.{str(self.run_number).zfill(3)}", str(f))]
        
        # collect data at all z position
        self.data = self.collect_zpos_data()
        
    def collect_zpos_data(self) -> pd.DataFrame:
        """Collects data from all z positions in self.zpos_list and returns a combined dataframe.

        Returns:
            pd.DataFrame: dataframe containing the data from all z positions
        """
        data = pd.DataFrame([])    
        for zpos in self.zpos_list:
            data = pd.concat([data, self.prepare_data(self.run_number, zpos)], ignore_index=True)
        return data
        
    def plot_emittance_at_zpos(self, zpos:int) -> None:
        """Plots the emittance at given z position

        Args:
            zpos (int): z position to be plotted
        """
        # get the figure from parent class
        fig = self._relative_momentum_plot("x", zpos)
        # save with settings of this class
        fig_name = self.plots_dir.joinpath(f"emittance_zpos_{zpos}.png")
        fig.savefig(fig_name, dpi=300, bbox_inches="tight")
        plt.close()
    
    def plot_emittance(self) -> None:
        """Iterates over all z positions in the directory and plots the emittance for each of them.
        """
        for zpos in self.zpos_list:
            self.logger.info(f"Plotting emittance at zpos={zpos}")
            self.plot_emittance_at_zpos(zpos)
        self.logger.info("Emittence plots done.")
    
    def plot_emittance_development(self):
        """Plots the development of the rms emittance along the trajectory (z).
        """
        # get the data
        df = self.get_Xemit(self.run_number)
        
        fig, ax = plt.subplots(figsize=(6,5))
        
        plt.plot(df["z"], df["epsilon"])
        
        plt.xlabel(f"$z$ [m]")
        plt.ylabel("Emittance (x rms) [mm mrad]")
        
        fig_name = self.plots_dir.joinpath(f"emittance_development.png")
        fig.savefig(fig_name, dpi=300, bbox_inches="tight")
        plt.close()
    
    def plot(self):
        """Plots the emittance for every z position in the directory and plots the development of rms emittance along the trajectory (z).
        """
        self.plot_emittance()
        self.plot_emittance_development()
    
def main():
    plotter = PlotEmittance("1", working_dir="GAUSS_v2\Final")
    plotter.plot()
    plotter.save_data()
    
if __name__ == '__main__':
    main()