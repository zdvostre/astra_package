import sys
sys.path.append("..")

import pandas as pd
import re
import numpy as np

from plot_analysis import plot_core
from common import adjust_settings

class RunCharacteristics(plot_core.PlotCore):
    def __init__(self, run_number=1, working_dir="", distribution_file="generator.in", run_file="photo_track.in", log_dir="log", log_file_name="plots_log.log", console_log=True, plots_dir="plots", logger_name=None, characteristics_dir="characteristics") -> None:
        super().__init__(working_dir, distribution_file, run_file, log_dir, log_file_name, console_log, plots_dir, logger_name, load_output_file=False)
        
        self.run_number = run_number
        
        self.characteristics_dir = self.working_dir.joinpath(characteristics_dir)
        self.characteristics_dir.mkdir(parents=True, exist_ok=True)
        
        self.beam_parameters = self.load_beam_parameters()
        self.gun_settings = self.load_gun_settings()
        
    def load_beam_parameters(self) -> pd.DataFrame:
        zpos_list = [int(f.name.split(".")[1]) for f in self.working_dir.iterdir() if f.is_file(
            ) and re.match(fr".*{self.run_file.stem}\.[0-9][0-9][0-9][0-9]\.{str(self.run_number).zfill(3)}", str(f))]
        
        parameters_dict = {}
        parameters_dict["zpos"] = zpos_list
        
        parameters_dict["init_Q_total"] = np.array([self._read_gun_setting("Q_total") for zpos in zpos_list]).astype(float)
        
        def get_from(from_df, property_name, zpos):
            try:
                return float(from_df.loc[from_df["z"] == zpos/100][property_name])
            except:
                return str(from_df.loc[from_df["z"] == zpos/100][property_name])
            
        parameters_dict["energy"] = [get_from(self.get_Zemit(self.run_number), "E", zpos) for zpos in zpos_list]
        parameters_dict["energy_spread"] = [get_from(self.get_Zemit(self.run_number), "E_rms", zpos) for zpos in zpos_list]
        parameters_dict["x_emit"] = [get_from(self.get_Xemit(self.run_number), "epsilon", zpos) for zpos in zpos_list]
        parameters_dict["y_emit"] = [get_from(self.get_Yemit(self.run_number), "epsilon", zpos) for zpos in zpos_list]
        parameters_dict["x_rms"] = [get_from(self.get_Xemit(self.run_number), "x_rms", zpos) for zpos in zpos_list]
        parameters_dict["y_rms"] = [get_from(self.get_Yemit(self.run_number), "y_rms", zpos) for zpos in zpos_list]
        parameters_dict["beam_size"] = [get_from(self.get_Zemit(self.run_number), "z_rms", zpos) for zpos in zpos_list]
        
        # calculate lost particles
        parameters_dict["active_ratio"] = []
        for zpos in zpos_list:
            df = self.get_at_zpos(self.run_number, zpos)
            total = df.shape[0]
            active = df.loc[df["flag"] > 0].shape[0]
            parameters_dict["active_ratio"] += [active/total]
            
        parameters_dict["Q_total"] = np.array(parameters_dict["init_Q_total"])*np.array(parameters_dict["active_ratio"])
        
        return pd.DataFrame.from_dict(parameters_dict)
    
    def _read_gun_setting(self, setting):
        run_file_loader = adjust_settings.ChangeSettings(self.run_file)
        distribution_file_loader = adjust_settings.ChangeSettings(self.distribution_file)
        
        if run_file_loader.find_setting(setting) is not None:
            return run_file_loader.read_setting(setting)
        elif distribution_file_loader.find_setting(setting) is not None:
            return distribution_file_loader.read_setting(setting)
        else:
            self.logger.warning(f"Setting {setting} was not found in neither run nor distribution file!")
            return None
    
    def load_gun_settings(self) -> pd.DataFrame:
        setting_dict = {}
        setting_to_read = ["IPart", "Q_total", 
                           "Dist_z", "sig_z", "Lz", "rz",
                           "Lt", "rt",
                           "Dist_pz",
                           "Dist_x", "sig_x", "C_sig_x", "Lx"
                           "S_pos(1)", "MaxB(1)",
                           "MaxE(1)", "Phi(1)", "C_pos(1)", "C_smooth(1)",
                           "MaxE(2)", "Phi(2)", "C_pos(2)", "C_numb(2)",
                           "MaxE(3)", "Phi(3)", "C_pos(3)", "C_numb(3)",
                           "MaxE(4)", "Phi(4)", "C_pos(4)", "C_numb(4)",
                           ]
        for setting in setting_to_read:
            setting_dict[setting] = [self._read_gun_setting(setting)]
        
        return pd.DataFrame.from_dict(setting_dict)
    
    def save_beam_parameters(self):
        filename = self.characteristics_dir.joinpath(f"RUN{self.run_number}_beam_characteristics.xlsx")
        self.logger.info(f"Beam parametres saved to: {filename}")
        self.beam_parameters.to_excel(filename)
        
    def save_gun_parameters(self):
        filename = self.characteristics_dir.joinpath(f"RUN{self.run_number}_gun_parameters.xlsx")
        self.logger.info(f"Gun parametres saved to: {filename}")
        self.gun_settings.to_excel(filename)
        
    def save(self):
        self.save_beam_parameters()
        self.save_gun_parameters()
        
                