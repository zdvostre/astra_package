import configparser
import pathlib


def load_config(config_file = "config/config.conf") -> configparser.ConfigParser:
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.read(get_config_file(config_file))
    return config

def get_config_file(config_file) -> pathlib.Path:
    return get_main_working_dir().joinpath(config_file)

def get_main_working_dir() -> pathlib.Path:
    # when configuration.py is loaded in other scripts, the directory differs and it is impossible to find the config.conf file
    # regardless the position of the script -> this functions finds the main direcotry and returns it
    cwd = pathlib.Path.cwd()
    while cwd.name.lower() != "astra_package":
        cwd = cwd.parent
    # if not found
    if cwd.name.lower() != "astra_package":
        raise Exception("Base directory was not found (should be called astra_package. Maybe it was renamed?")
    return cwd

def get_config(config_file = "config/config.conf") -> configparser.ConfigParser:
    return load_config(config_file)
