import sys
sys.path.append("..")

import shutil
import pathlib

from common import core, astra_process

class OptimalizationCore(core.Core):
    def __init__(self, working_dir="", distribution_file="generator.in", run_file="photo_track.in", log_dir="log", log_file_name="log.log", console_log=False) -> None:
        super().__init__(working_dir, distribution_file, run_file, log_dir, log_file_name, console_log)
        self.base_dir = self.working_dir.joinpath("BASE")
        self.distribution_file = distribution_file
        self.run_file = run_file
        
        self.check_base_directory()
        
    def check_base_directory(self):
        # will raise an exception if any important files are missing in the BASE directory
        self.base_dir_process = astra_process.AstraProcess(working_dir=self.base_dir,
                                        distribution_file=self.base_dir.joinpath(self.distribution_file),
                                        run_file=self.base_dir.joinpath(self.run_file))
    
    def make_new_scan_dir(self, scanning_parameter:str) -> pathlib.Path:
        dest = self.working_dir.joinpath(f"SCAN_{scanning_parameter}")
        shutil.copytree(self.base_dir, dest)
        return pathlib.Path(dest)