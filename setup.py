import configparser
import pathlib
from setuptools import setup

# setup(
#    name='AstraFramework',
#    version='0.1.0',
#    author='Zdenek Vostrel',
#    author_email='zdenek.vostrel@cern.ch',
#    packages=['astra', 'common', "config", "plot_analysis", "optimalization"],
#    install_requires=[
#        "setuptools",
#        "pandas",
#        "numpy",
#        "argparse",
#        "matplotlib",
#        "pathlib",
#        "configparser"
#    ],
#    )

def setup_config():
    config = configparser.ConfigParser()
    config["Paths"] = {
        "base_dir": pathlib.Path.cwd(),
        "data_dir": pathlib.Path.cwd().joinpath("data")
    }   
    
    with open("config/config.conf", "w") as f:
        config.write(f)

def main():
    setup_config() 

if __name__ == "__main__":
    main()
