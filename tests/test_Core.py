import sys
sys.path.append("..")

import unittest
import shutil
import pathlib

from config import configuration
from common import core

testing_dir_name = ".TESTS"

def get_main_dir() -> pathlib.Path:
    return configuration.get_main_working_dir()
target_dir = get_main_dir().joinpath(configuration.load_config()["Paths"]["data_dir"]).joinpath(testing_dir_name)

class TestConfig(unittest.TestCase):
    def setUp(self) -> None:
        return super().setUp()
    
    @classmethod
    def setUpClass(csl) -> None:
        for i in range(3):
            source_file = pathlib.Path.cwd().joinpath("tests/fixtures/test_core_run_number"+str(i+1))
            temp_file = target_dir.joinpath(source_file.name)
            shutil.copytree(source_file, temp_file) 
    
    @classmethod
    def tearDownClass(cls) -> None:
        shutil.rmtree(pathlib.Path.cwd().joinpath("data").joinpath(testing_dir_name).joinpath("log"))
        
        for i in range(3):
            source_file = pathlib.Path.cwd().joinpath("tests/fixtures/test_core_run_number"+str(i+1))
            shutil.rmtree(target_dir.joinpath(source_file.name))
            
        shutil.rmtree(pathlib.Path.cwd().joinpath("data").joinpath(testing_dir_name))
            
    def test_setup_logger(self):
        c = core.Core(working_dir=testing_dir_name)
        c.logger.info("Testing message.")
        self.assertTrue(c.log_file.exists())
        
    def test_get_first_free_run_number(self):
        try:
            c = core.Core(working_dir=testing_dir_name+"/test_core_run_number1")
            run_number = c.get_first_free_run_number()
            self.assertEqual(run_number, 1)
            c = core.Core(working_dir=testing_dir_name+"/test_core_run_number2")
            run_number = c.get_first_free_run_number()
            print(run_number)
            self.assertEqual(run_number, 27)
            c = core.Core(working_dir=testing_dir_name+"/test_core_run_number3")
            run_number = c.get_first_free_run_number()
            print(run_number)
            self.assertEqual(run_number, 3)
        finally:
            del c

    
