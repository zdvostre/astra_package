import sys
sys.path.append("..")

import unittest
import pathlib

from config import configuration

class TestConfig(unittest.TestCase):
    def test_get_main_working_dir(self):
        main_working_dir = pathlib.Path.cwd()
        while main_working_dir.name.lower() != "astra_package":
            main_working_dir = main_working_dir.parent
        self.assertEqual(main_working_dir, configuration.get_main_working_dir())
        
    def test_get_config_file(self):
        conf_file = configuration.get_config_file("tests/fixtures/test_config.conf")
        self.assertEqual(conf_file, configuration.get_main_working_dir().joinpath("tests/fixtures/test_config.conf"))
        
        
    def test_get_config(self):
        conf = configuration.get_config("tests/fixtures/test_config.conf")
        
        self.assertTrue("Paths" in conf.sections())
        
        self.assertEqual("/root/users/programming/astra_package", conf["Paths"]["base_dir"])
        self.assertEqual("/root/users/programming/astra_package/data", conf["Paths"]["data_dir"])
        