def main(*args, **kwargs):
    print(f"Total number of args: {len(args)}.")
    print(f"Total number of kwargs: {len(kwargs)}.")
    for arg in args:
        print(arg)
    for key,arg in kwargs:
        print(f"{key} : {arg}")