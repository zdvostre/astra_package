import sys
sys.path.append("..")

import unittest
import shutil
import pathlib

from common import adjust_settings

class TestAdjustSettings(unittest.TestCase):
    def test_read_setting(self):
        source_file = pathlib.Path.cwd().joinpath("tests/fixtures/test_adjust_settings.txt")  
        cs = adjust_settings.ChangeSettings(source_file)
        
        run_number = cs.read_setting("RUN") #single argument on line, ends with new line
        self.assertEqual(run_number, "14")
        
        LPSCH = cs.read_setting("LSPCH") #single argument on line, ends with coma
        self.assertEqual(LPSCH, "T")
        
        Nrad = cs.read_setting("Nrad") #multiple on line, the first one
        self.assertEqual(Nrad, "30")
        
        Spos = cs.read_setting("S_pos(3)") #multiple on line, in the middle
        self.assertEqual(Spos, "5.70")
        
        Ssmooth = cs.read_setting("S_smooth(3)") #multiple on line, the last one
        self.assertEqual(Ssmooth, "10")
        
        Bfield = cs.read_setting("File_Bfield(1)") #reading longer line
        self.assertEqual(Bfield, "'DOUBLE_NEW_SOL.dat'")
        
    def test_change_setting(self):
        source_file = pathlib.Path.cwd().joinpath("tests/fixtures/test_adjust_settings.txt")
        temp_file = source_file.parent.joinpath("temp_"+source_file.name)
        shutil.copy(source_file, temp_file) 
        
        cs_CHANGE = adjust_settings.ChangeSettings(temp_file)
        cs_CHANGE.update("RUN", 1)
        cs_CHANGE.update("File_Efield(1)", "'SwissFEL_Feb2022.dat'")
        
        cs_READ = adjust_settings.ChangeSettings(temp_file)
        run_number = cs_READ.read_setting("RUN") 
        self.assertEqual(run_number, "1")  
        run_number = cs_READ.read_setting("File_Efield(1)") 
        self.assertEqual(run_number, "'SwissFEL_Feb2022.dat'")
        
        temp_file.unlink()
        
        