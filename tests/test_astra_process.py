import sys
sys.path.append("..")

import unittest
import shutil
import pathlib

from config import configuration
from common import astra_process
from common import adjust_settings

testing_dir_name = ".TESTS"

def get_main_dir() -> pathlib.Path:
    return configuration.get_main_working_dir()

src_dir = get_main_dir().joinpath("tests/fixtures/test_astra_process_perform_checks1")
target_dir = get_main_dir().joinpath(configuration.load_config()["Paths"]["data_dir"]).joinpath(testing_dir_name)


class TestConfig(unittest.TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        
    @classmethod
    def tearDownClass(cls) -> None:
        if target_dir.is_dir():
            for d in [d for d in target_dir.iterdir() if d.is_dir() and ("TEST_AstraProcess_" in d.name)]:
                shutil.rmtree(d)
        shutil.rmtree(pathlib.Path.cwd().joinpath("data").joinpath(testing_dir_name))
            
        
    def test_perform_checks_none_missing(self):
        # prepare directory
        l_testing_dir_name = pathlib.Path(testing_dir_name).joinpath(f"TEST_AstraProcess_perform_checks1")
        l_target_dir = target_dir.joinpath("TEST_AstraProcess_perform_checks1")
        shutil.copytree(src_dir,l_target_dir)
        try:
           ap = astra_process.AstraProcess(working_dir=l_testing_dir_name)
        except Exception:
            self.fail("Exception raised unexpectedly. All file should be present.") 
    
    def test_perform_checks_one_missing(self):
        # prepare directory
        l_testing_dir_name = pathlib.Path(testing_dir_name).joinpath(f"TEST_AstraProcess_perform_checks2")
        l_target_dir = target_dir.joinpath("TEST_AstraProcess_perform_checks2")
        shutil.copytree(src_dir,l_target_dir)
        
        for f in [f for f in l_target_dir.iterdir() if f.is_file() and (f.suffix == ".exe" or f.suffix == ".in")]:
            # store original suffix
            orig_suffix = f.suffix
            # rename to a different suffix
            f = f.rename(f.with_suffix(".temp"))
            # check if raises expetion
            try:
                ap = astra_process.AstraProcess(working_dir=l_testing_dir_name)
                self.fail(f"Exception not raised even though file {f.with_suffix(orig_suffix).name} is missing.")
            except Exception:
                # change to original suffix
                f.rename(f.with_suffix(orig_suffix))
    
    def test_run_generator(self):
        # prepare directory
        l_testing_dir_name = pathlib.Path(testing_dir_name).joinpath(f"TEST_AstraProcess_run_generator")
        l_target_dir = target_dir.joinpath("TEST_AstraProcess_run_generator")
        shutil.copytree(src_dir,l_target_dir)
        # initialize
        ap = astra_process.AstraProcess(working_dir=l_testing_dir_name)
        # change generator program to an python file
        ap.generator_program = ap.working_dir.joinpath("input_file.py")
        # check if distribution file is correct
        self.assertEqual(l_target_dir.joinpath("generator.in"), ap.distribution_file)
        # check if can be run
        try:
            ap.run_generator()
        except Exception:
            self.fail("Generator programme should be running, but it is not.")
            
    def test_run_astra(self):
        # prepare directory
        l_testing_dir_name = pathlib.Path(testing_dir_name).joinpath(f"TEST_AstraProcess_run_astra")
        l_target_dir = target_dir.joinpath("TEST_AstraProcess_run_astra")
        shutil.copytree(src_dir,l_target_dir)
        # initialize
        ap = astra_process.AstraProcess(working_dir=l_testing_dir_name)
        # change generator program to an python file
        ap.astra_program = ap.working_dir.joinpath("input_file.py")
        # check if distribution file is correct
        self.assertEqual(l_target_dir.joinpath("photo_track.in"), ap.run_file)
        # check if can be run
        try:
            ap.run_astra()
        except Exception:
            self.fail("Generator programme should be running, but it is not.")
    
    def test_change_parameter(self):
        # prepare directory
        l_testing_dir_name = pathlib.Path(testing_dir_name).joinpath(f"TEST_AstraProcess_change_parameter")
        l_target_dir = target_dir.joinpath("TEST_AstraProcess_change_parameter")
        shutil.copytree(src_dir,l_target_dir)
        ap = astra_process.AstraProcess(working_dir=l_testing_dir_name)
        
        ap.change_parameter("RUN", "100")
        self.assertTrue(adjust_settings.ChangeSettings(ap.run_file).read_setting("RUN"), "100")
        
        ap.change_parameter("Dist_x", '"uniform"')
        self.assertTrue(adjust_settings.ChangeSettings(ap.distribution_file).read_setting("Dist_x"), '"uniform"')
        
    def test_increase_run_number(self):
         # prepare directory
        l_testing_dir_name = pathlib.Path(testing_dir_name).joinpath(f"TEST_AstraProcess_increase_run_number")
        l_target_dir = target_dir.joinpath("TEST_AstraProcess_increase_run_number")
        shutil.copytree(src_dir,target_dir.joinpath(l_target_dir))
        ap = astra_process.AstraProcess(working_dir=l_testing_dir_name)
        
        ap.increase_run_number()
        self.assertTrue(adjust_settings.ChangeSettings(ap.run_file).read_setting("RUN"), "2")
        ap.increase_run_number()
        self.assertTrue(adjust_settings.ChangeSettings(ap.run_file).read_setting("RUN"), "3")
    
    def test_set_run_number(self):
        # prepare directory
        l_testing_dir_name = pathlib.Path(testing_dir_name).joinpath(f"TEST_AstraProcess_set_run_number")
        l_target_dir = target_dir.joinpath("TEST_AstraProcess_set_run_number")
        shutil.copytree(src_dir, l_target_dir)
        ap = astra_process.AstraProcess(working_dir=l_testing_dir_name)
        ap.set_run_number("10")
        self.assertTrue(adjust_settings.ChangeSettings(ap.run_file).read_setting("RUN"), "10")
        ap.set_run_number("196")
        self.assertTrue(adjust_settings.ChangeSettings(ap.run_file).read_setting("RUN"), "196")