import sys
sys.path.append("..")

import pathlib
import shutil
import numpy as np

from common import chain_core
from astra import parallel_scan

class ChainScan(chain_core.ChainCore):
    def __init__(self, working_dir="", distribution_file="generator.in", run_file="photo_track.in", log_dir="log", log_file_name="log.log", console_log=True, logger_name=None, n_parallel=5) -> None:
        """Class for parallel chain scan.

        Args:
            working_dir (str | pathlib.Path, optional): Working dir in which to perform the scan. Defaults to "".
            distribution_file (str | pathlib.Path, optional): Name of the distribution file. Defaults to "generator.in".
            run_file (str | pathlib.Path, optional): Name of the run file. Defaults to "photo_track.in".
            log_dir (str | pathlib.Path, optional): Path to the log directory. Defaults to "log".
            log_file_name (str | pathlib.Path, optional): Name of the log file. Defaults to "log.log".
            console_log (bool, optional): Option if to log to console. Defaults to False.
            logger_name (str | pathlib.Path, optional): Name of the logger. If set to None, new logger will be created. Defaults to None.
            n_parallel (int, optional): Number of parallel runs. Defaults to 5.
        """
        super().__init__(working_dir, distribution_file, run_file, log_dir, log_file_name, console_log, logger_name, n_parallel=n_parallel)
        
        self.scan_parameters = []
        
    def add_scanning_parameter(self, parameter:str, values=None, start=None, end=None, step=None, n_steps=None) -> None:
        """Method to add a scanning parameter for the parallel scan.

        Args:
            parameter (str): Name of the parameter.
            values (list, optional): List of the values to be used. Defaults to None.
            start (float, optional): Start of the scan. Does not have to be provided if values are specifically set. If this is set, other optional parameters are not needed to be specified. Defaults to None.
            end (float, optional): End of the scan. Does not have to be provided if values are specifically set. Defaults to None.
            step (float, optional): Step of the scan. Does not have to be provided if values are specifically set or n_steps are specified. Defaults to None.
            n_steps (int, optional): Number of steps to be performed. Does not have to be provided if values are specifically set or step is specified. Defaults to None.
        """
        # use parent method for initialisation
        parameter_dictionary = self._parameter_values_initialization(parameter, values, start, end, step, n_steps)
        
        self.logger.info(f"Adding scanning parameter: {parameter_dictionary['parameter']}, start: {parameter_dictionary['start']}, end: {parameter_dictionary['end']} (number of steps: {parameter_dictionary['n_steps']}).")
        # add it to the set of layers
        self.scan_parameters += [parameter_dictionary]
        
    def setup_scan_dir(self, preset_parameters:dict) -> pathlib.Path:
        """Method to setup the scan dir for the prese parameters. Each layer mean new subdirectory structure.

        Args:
            preset_parameters (dict): Dictionary of parameters from layers.

        Returns:
            pathlib.Path: Path to the scan directory.
        """
        scan_dir = pathlib.Path(self.working_dir)
        log_string = "Setting up directory for: "
        for parameter in preset_parameters.keys(): 
            value = preset_parameters[parameter]
            log_string += (parameter + " = " + str(value) + "; ")   
            # make sure the parameter is set correctly
            self.base_dir_process.change_parameter(parameter, value)
            # add layer to the directory structure
            scan_dir = scan_dir.joinpath(parameter+"_"+str(value))
        
        # make sure the parent directory exists for safe copying
        scan_dir.parent.mkdir(parents=True, exist_ok=True)
        # copy the scan directory
        shutil.copytree(self.base_dir, scan_dir, dirs_exist_ok=True)
        self.logger.info(log_string)
        self.logger.info("Location: " + str(scan_dir))
        return scan_dir
        
    def run(self):
        """Method to run the chain parallel scan.
        """
        # initialize values from different layers
        layer_number = 0
        total_scans = np.prod([len(layer["values"]) for layer in self.main_layers])
        self.logger.info("Total number of " + str(total_scans) + " scans will be performed.")
        simulation_preset_parameters = [{} for scan in range(total_scans)]
        while layer_number < len(self.main_layers):
            n_same_sequence = total_scans//np.prod([layer["n_steps"] for layer in self.main_layers[:layer_number+1]])
            n_filled = 0
            main_layer = self.main_layers[layer_number]
            while n_filled < total_scans:
                for i in range(main_layer["n_steps"]):
                    for n in range(n_same_sequence):
                        for layer in self.get_same_index_layers(main_layer["layer_idx"]):
                            simulation_preset_parameters[n_filled+n][layer["parameter"]] = layer["values"][i]
                    n_filled += n_same_sequence
            layer_number += 1
            
        # log the presets
        self.write_presets_to_output_file(simulation_preset_parameters)
        # run the scans
        scans_completed = 0
        for preset in simulation_preset_parameters:
            scan_dir = self.setup_scan_dir(preset)
            ps = parallel_scan.ParallelScan(working_dir=scan_dir,
                                    n_parallel=self.n_parallel,
                                    console_log=False
                                    )
            self.logger.info(f"Running scan {scans_completed+1}/{total_scans}.")
            for parameter in self.scan_parameters:
                ps.add_scanning_parameter(parameter["parameter"], values=parameter["values"])
                
            ps.run_scan()
            self.logger.info("Scan completed!")
            ps.plot()
            self.logger.info("Plots complete!")
            scans_completed += 1
            
                
        