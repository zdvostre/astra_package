import sys
sys.path.append("..")

from common import astra_process

# OUTDATED -> should not be used

class BasicScanner(astra_process.AstraProcess):
    def __init__(self, scanning_parameter, start, end, step, working_dir="", distribution_file="generator.in", run_file="photo_track.in") -> None:
        super().__init__(working_dir, distribution_file, run_file)

        self.scanning_parameter = scanning_parameter
        # TODO: Check if makes sense :)
        self.start = start
        self.end = end
        self.step = step

    def run_scan(self):
        curent_value = self.start

        while curent_value <= self.end:
            self.change_parameter(self.scanning_parameter, curent_value)

            self.run_generator()  # TODO: run only if necessary
            self.run_astra()

            curent_value += self.step
            self.increase_run_number()


def main():
    scan = BasicScanner("Phi(2)", "photo_track.in", -5.0, 10.0, 0.5,
                        working_dir='C:\\Users\\zdvostre\\Programming\\astra_package\\data\\First_cavity_phase')
    scan.run_scan()


if __name__ == '__main__':
    main()
