import sys 
sys.path.append("..")

import argparse
import json
import shutil
import threading
import pandas as pd
import time

from common import astra_process, core, file_loader

class Runner(core.Core):
    def __init__(self, set_parameters:dict, working_dir="", distribution_file="generator.in", run_file="photo_track.in", run_number=1, log_dir="log", log_file_name="log.log", console_log=False, logger_name=None) -> None:
        super().__init__(working_dir, distribution_file, run_file, log_dir, log_file_name, console_log, logger_name)
        
        self.run_number = run_number

        self.distribution_file, self.run_file = distribution_file, run_file

        self.set_parameters = set_parameters

        self.process = astra_process.AstraProcess(working_dir=working_dir, distribution_file=distribution_file, run_file=run_file,
                                                  log_dir=f"log\\RUN_{str(self.run_number)}", log_file_name=f"run{str(self.run_number)}_log.log", run_number=run_number)

        self.output_handler = self.get_output_file_handler(self.working_dir)


    def run(self) -> list:
        # get lock for file changing and astra runs
        # THIS HAS TO BE REALEASED LATER IN THIS METHOD
        self.astraLock.acquire()
        
        
        self.process.change_parameter("FNAME", f"\"gun.ini\"")
        self.process.change_parameter("Distribution", f"\"gun.ini\"")

        for key in self.set_parameters.keys():
            self.process.change_parameter(key, self.set_parameters[key])
        
        self.process.run_generator(shell=False)
        self.save_initial_distribution()
        
        # THIS RELEASES THE LOCK (after 2 s) acquired at the start of this method
        # it must be done this way to allow ASTRA to be started, but we do not want to wait till ASTRA finished
        def release_lock():
            # this is to give time to astra to start properly!
            time.sleep(2)
            self.astraLock.release()
        thread = threading.Thread(target=release_lock)
        thread.start()
        
        self.process.run_astra(shell=False)
        
        output = self.update_run_output_file()
        
        return output

    def save_initial_distribution(self):
        # get initial distribution and save it
        shutil.copy(
            self.working_dir.joinpath(self.process.get_parameter_value("FNAME")[1:-1]),
            self.working_dir.joinpath(f"{str(self.run_file).split('.')[0]}.0000.{str(self.run_number).zfill(3)}")   
        )

    def update_run_output_file(self) -> list:
        def check_lost_particles():
            fileLoader = file_loader.FileLoader(working_dir=self.working_dir, distribution_file=self.distribution_file, run_file=self.run_file)
            df = fileLoader.get_at_zpos(self.run_number, "end")
            total_particles = df.shape[0]
            active_particles = df.loc[df["flag"] > 0].shape[0]
            lost_particles = total_particles - active_particles
            active_ratio = active_particles / total_particles
            
            # if lost_particles != 0:
            #     self.logger.warning(f"Particles lost in run {self.run_number}: {self.lost_particles} lost out of total of {total_particles} particles ({100 - round(active_ratio*100)} %).")
            # else:
            #     self.logger.info("Checked particle status. No particles lost.")
            
            return [total_particles, active_particles, active_ratio, lost_particles]
        particles_status = check_lost_particles()

        output = {"Run number":[self.run_number]}
        output.update({key: self.set_parameters[key] for key in self.set_parameters.keys()})
        output.update({"Active ratio": particles_status[2]})

        try:
            self.output_handler.add_output(output)
        except:
            pass
        return output


def main():
    parser = argparse.ArgumentParser(
        description='Script for changing a parameter and running the astra with changed parametres.')

    parser.add_argument('--working_dir', type=str, action='store', required=True,
                        help='Current working dir (with data).')
    parser.add_argument('--distribution_file', type=str, action='store', default="generator.in",
                        help='Generator distribution input file (default generator.in).')
    parser.add_argument('--run_file', type=str, action='store', default="photo_track.in",
                        help='Astra run input file (default photo_track.in).')
    parser.add_argument('--run_number', type=int, action='store', default=1,
                        help='Run number to be set (default = 1).')
    parser.add_argument('--set_parameters', type=str, action='store', required=True,
                        help='Name of parameter to be changed.')

    parsed = parser.parse_args()

    runner = Runner(json.loads(parsed.set_parameters.replace("'",'"')),
                    working_dir=parsed.working_dir,
                    distribution_file=parsed.distribution_file,
                    run_file=parsed.run_file,
                    run_number=parsed.run_number,
                    )
    runner.run()


if __name__ == '__main__':
    main()
