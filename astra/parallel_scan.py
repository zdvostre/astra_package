import sys
sys.path.append("..")

import pathlib
import time
import argparse
import subprocess
import configparser
import shutil
import math
import filelock

from common import core
from plot_analysis import plot_properties

class ParallelScan(core.Core):
    def __init__(self, scanning_parameter=None, start=None, end=None, step=None, working_dir="", distribution_file="generator.in", run_file="photo_track.in", n_parallel=3, log_dir="log", initial_run_number=None, console_log=True) -> None:
        super().__init__(working_dir, distribution_file, run_file, log_dir, "parallel_scan.log", console_log=console_log)

        self.initial_run_number = int(initial_run_number) if str(initial_run_number).strip().lower() != "none" else self.get_first_free_run_number()

        self.scanning_parameters = []
        
        self.output_handler = self.get_output_file_handler(self.working_dir)
        self.logger.info(f"Using output file: \n {self.output_handler.output_file}")
        self.verify_initial_run_number()
        
        self.n_paralel = int(n_parallel)
        self.n_steps = None

        self.mean_run_time = 0
        
        if isinstance(scanning_parameter, list):
            for i in range(len(scanning_parameter)):
                self.add_scanning_parameter(scanning_parameter[i], start[i], end[i], step[i])
        else:
            if scanning_parameter is not None:
                self.add_scanning_parameter(scanning_parameter, start=start, end=end, step=step)
        
    def add_scanning_parameter(self, parameter, values=None, start=None, end=None, step=None, n_steps=None, **kwargs):
        parameter_dictionary = self._parameter_values_initialization(parameter, values, start, end, step, n_steps)
        n_steps = parameter_dictionary["n_steps"]
        
        if self.n_steps is None:
            self.n_steps = n_steps 
        if n_steps != self.n_steps:
            raise Exception(f"Parameter {parameter} was set with different number of steps ({n_steps}) than it was expected to ({self.n_steps}).")
        
        self.scanning_parameters += [parameter_dictionary]
 
        
        self.logger.info(f"Added scanning parameter {parameter_dictionary['parameter']}")
        self.logger.info(f"Parameter scan starting at: {parameter_dictionary['start']}")
        self.logger.info(f"Parameter scan ending at: {parameter_dictionary['end']}")
        if parameter_dictionary['step'] is not None: 
            self.logger.info(f"Scanning step set to: {parameter_dictionary['step']} (number of steps: {parameter_dictionary['n_steps']})")
        else:
            self.logger.info(f"Scan will be performed for values: " + str(value) for value in parameter_dictionary['values'])
        
    def verify_initial_run_number(self):
        """Checks if run number is free. If not, the run number is set to first free run number.
        
        Run number is considered as free, if all run number equal or higher than this are not taken.
        """
        # check if inital run number is higher than the first free number
        if self.get_first_free_run_number() < self.initial_run_number:
            # if not, set it to the first free run number
            self.logger.warning(f"Run number {self.initial_run_number} has already been taken!")
            self.initial_run_number = self.get_first_free_run_number()
            self.info(f"Initial run number was set to the first free run number, which is {self.initial_run_number}.")
    
    def run_scan(self) -> None:
        """Runs the scan.
        """
        if self.n_steps is None:
            self.logger.info("No scanning parameters were set. The scan will not be performed.")
            return
        else:
            self.logger.info(f"Total number of {self.n_steps} simulations will be performed, number of parallel simulations is set to {self.n_paralel}.")
        self.logger.info(f"This scan will be performed in runs {self.initial_run_number} -- {self.initial_run_number + self.n_steps-1}.")
        
        # Setup of the scan properties
        now_running = 0
        processes = []
        self.n_finished = 0
        run_number = self.initial_run_number
        
        # Setup start time for the estimated finish time
        self.start_time = time.time()
        
        while self.n_finished < self.n_steps:
            # Iterate iver the current running processes and check, if they ended
            for p,n,process_start_time in processes:
                if p.poll() is not None: # True if process finished
                    processes.remove([p,n,process_start_time]) # remove from running processes
                    self.update_mean_runtime(time.time() - process_start_time)# Must be called before the self.n_finished is updated
                    # update scan properties
                    now_running -= 1
                    self.n_finished += 1
                    self.logger.info(f"Process with run number {n} finished after {time_get_h_m(time.time() - process_start_time)}.")
                    
            # submit new processes until the limit is reached    
            while now_running < self.n_paralel and self.n_steps > self.n_finished + now_running:
                # this is to allow to submit the process only if the astra lock is released
                with self.astraLock.acquire():
                    pass
                p = subprocess.Popen(["python", pathlib.Path(self.config["Paths"]["base_dir"]).joinpath("astra/run.py"), 
                                    "--working_dir", str(self.working_dir), 
                                    "--distribution_file", self.distribution_file.name, 
                                    "--run_file", self.run_file.name, 
                                    "--run_number", str(run_number), 
                                    "--set_parameters", str(
                                        {self.scanning_parameters[i]["parameter"]: self.scanning_parameters[i]["values"][run_number-self.initial_run_number]
                                            for i in range(len(self.scanning_parameters))
                                            }
                                        ).replace("'",'"'),
                                        ], 
                                    shell=True, stdin=subprocess.PIPE)
                self.logger.info(f"Submitted new process with run number {run_number}.")
                processes = processes + [[p, run_number, time.time()]] # update list of running properties
                # update run properties
                run_number += 1
                now_running += 1
                # this is to prevent warning then all try to write at once to the same NORAN file and correct astraLock acquiry
                time.sleep(1) 
            
            # Logging section
            run_time = time.time() - self.start_time
            self.logger.info(f"Total: {self.n_steps} :: Now running: {now_running}/{self.n_paralel} :: Finished: {self.n_finished} :: Waiting: {self.n_steps - self.n_finished - now_running} :: Time running: {time_get_h_m(run_time)} :: Estimated time to finish: {self.get_estimated_time_to_finish()}")
            
            # if there are processes running, wait until new check is made   
            if now_running != 0: 
                time.sleep(10)
                
        self.logger.info("Scan completed.")
        
        self.check_lost_particles()
        
    def check_lost_particles(self):
        """Check for particles lost in the runs and logs it.
        """
        # load data from output file, where lost particles are listed
        df = self.get_output_file_handler(self.working_dir).output_content
        df = df.sort_values(by=["Run number"])
        n = self.initial_run_number
        
        self.logger.info("Checking for lost particles.")
        # iterate over the runs in this parallel scan
        while n < self.initial_run_number + self.n_steps:
            try:
                # get the active particle ratio
                active_ratio = float(df.loc[df["Run number"] == n]["Active ratio"])
            except:
                try:
                    # if multiple runs with same run number, the mean is taken and warning is trigerred
                    active_ratio = df.loc[df["Run number"] == n]["Active ratio"].mean()
                    self.logger.warning(f"Multiple runs with run number {n}. Taki   ng mean value of the ratio.")
                except:
                    # trigger warning if the run number cant be found in the output file and continue
                    self.logger.warning(f"No run with run number {n} in the output file! Skipping.")
                    n += 1
                    continue
            if math.isnan(active_ratio):
                self.logger.critical(f"Active ratio for run number {n} is NaN. Please investigate.")
            elif active_ratio != 1:
                # trigger warning where there were particles lost
                self.logger.warning(f"Particles lost in run {n}: {100 - round(active_ratio*100)} % of particles lost.")
            else:
                self.logger.info(f"No particles lost in run {n}.")
            n += 1
        self.logger.info("Check complete.")
        
    def log_config_file(self, config_file) -> None:
        """Copies the config_file to the log directory

        Args:
            config_file (str, path): Config file to be logged.
        """
        # make sure it is a Path
        config_file = pathlib.Path(config_file)
        
        # copy the file
        config_file_log = self.log_dir.joinpath(config_file.name)
        shutil.copy(config_file, config_file_log)
        
        self.logger.info(f"Config file backed to: {config_file_log}")      
    
    def update_mean_runtime(self, time:int) -> None:
        """Updates the mean run time.

        Args:
            time (int): Time after the running process finished.
        """
        # Must be called before the self.n_finished is updated
        self.mean_run_time = (self.mean_run_time*self.n_finished + time)/(self.n_finished+1)
        
    def get_estimated_time_to_finish(self) -> str:
        """Calculates the estimated time to finish.

        Returns:
            str: XXh XXm format of time to finish.
        """
        # if no processes finished yet
        if self.mean_run_time == 0:
            return "(no estimate)"
        # estimate the run time by mean run time times number of parallel packs to be ru
        estimated_run_time = self.mean_run_time*(self.n_steps//self.n_paralel)
        # if the last parallel pack is not full, in is not counted in the previous line and thus mean run time must be added
        estimated_run_time += self.mean_run_time if self.n_steps%self.n_paralel != 0 else 0
        return time_get_h_m(estimated_run_time - (time.time() - self.start_time))
        
    def plot(self, plot_everything=True) -> None:
        self.logger.info("Starting plotter.")
        for parameter_dict in  self.scanning_parameters:
            parameter = parameter_dict["parameter"]
            plots_dir = "plots/"+parameter.strip()
            
            # setup unit 
            unit = None # this makes it set automatically
            scale = 1
            # unless Lt, where rescalling is needed
            if "Lt" in parameter:
                unit = "ps" 
                scale = 1000
            
            self.logger.info(f"Plotting final properties dependance on {parameter}.")
            self.logger.info(f"Unit of scanning parameter {parameter} is set to \" {unit} \".")
            
            # correctly setup plotter and plot final properties
            if plot_everything:
                plotter = plot_properties.PlotProperties(parameter, working_dir=self.working_dir, 
                                                        console_log=False,  
                                                        scanning_parameter_unit=unit, 
                                                        parameter_scaling=scale,
                                                        logger_name=self.logger.name,
                                                        plots_dir=plots_dir)
            else:
                plotter = plot_properties.PlotProperties(parameter, working_dir=self.working_dir, 
                                                        console_log=False, 
                                                        max_value=self.end, min_value=self.start, 
                                                        min_run_number=self.initial_run_number, 
                                                        max_run_number=self.initial_run_number+self.n_steps-1, 
                                                        scanning_parameter_unit=unit, 
                                                        parameter_scaling=scale,
                                                        logger_name=self.logger.name,
                                                        plots_dir=plots_dir)
            plotter.plot() 
        
def time_get_h_m(seconds):
    return f"{int(seconds//3600)}h {int((seconds//60)%60)}m"
    
            
def main():
    parser = argparse.ArgumentParser(
        description='Script for performing a parallel scan of given parameter. To run, provide a file with settings.')

    parser.add_argument('--setting_file', type=str, action='store', default="parallel.conf",
                        help='Input file with settings for the parallel run (default parallel.conf).')
    parsed = parser.parse_args()
    
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.read(parsed.setting_file)
    
    ps = ParallelScan(config["General"]["scanning_parameter"], 
                      config["General"]["start"], config["General"]["end"], 
                      config["General"]["step"], config["General"]["working_dir"], 
                      config["General"]["distribution_file"], config["General"]["run_file"],
                      config["General"]["n_parallel"], 
                      initial_run_number=config["General"]["initial_run_number"])
    ps.log_config_file(parsed.setting_file)
    ps.run_scan()
    ps.plot()
    
if __name__ == '__main__':
    main()