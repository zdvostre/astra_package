import sys
sys.path.append("..")

from optimalization import basic_optimalization

def main():
    optimizer = basic_optimalization.BaseOptimalization(working_dir="UNIFORM_TOPUP\FULL_OPTIMALISATION_5nC")
    
    optimizer.add_optimalization_parameter("x_emit", 1000)
    optimizer.add_optimalization_parameter("energy_spread", 1)
    
    # optimizer.add_scanning_parameter("Phi(1)", -40, 40, step=5)
    optimizer.add_scanning_parameter("MaxB(1)", 0.24, 0.27, step=0.005)
    optimizer.add_scanning_parameter("Phi(2)", -40, 40, step=5)
    optimizer.add_scanning_parameter("Phi(3)", -40, 40, step=5)
    optimizer.add_scanning_parameter("Phi(4)", -40, 40, step=5)
    optimizer.add_scanning_parameter("Lx", 1.5, 3, step=0.1)
    optimizer.add_scanning_parameter("Q_total", 1, 7, step=2, only_scan=True)
    
    optimizer.optimize()

if __name__ == "__main__":
    main()