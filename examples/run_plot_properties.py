import sys
sys.path.append("..")

import argparse
import configparser

from plot_analysis import plot_core

working_dir = r"CHARGE_SCAN_GAUSS\GaussScan_LT_0_005"

start = -2000
end = 1000
scanning_parameter_unit = "nC"
parameter_scaling = 1
min_run_number = 1
max_run_number = None

def main():
    parser = argparse.ArgumentParser(
        description='Script for performing a parallel scan of given parameter. To run, provide a file with settings.')

    parser.add_argument('--setting_file', type=str, action='store', default="plot_properties.conf",
                        help='Input file with settings for the parallel run (default parallel.conf).')
    parsed = parser.parse_args()
    
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.read(parsed.setting_file)
    
    ps = plot_core.PlotProperties("end", working_dir, 
                                  min_value=start, 
                                  max_value=end, 
                                  scanning_parameter_unit=scanning_parameter_unit, 
                                  min_run_number=min_run_number, 
                                  max_run_number=max_run_number, 
                                  parameter_scaling=parameter_scaling
                                  )
    ps.plot()
    
if __name__ == '__main__':
    main()