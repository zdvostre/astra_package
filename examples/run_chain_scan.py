import sys
sys.path.append("..")

from astra import chain_scan

working_dir = r""

def main():
    chain = chain_scan.ChainScan(working_dir)
    
    chain.add_layer("sig_clock", start=0.001, end=0.02, n_steps=10)
    chain.add_scanning_parameter("MaxB(1)", start=0.22, end=0.27, step=0.005)
    
if __name__ == '__main__':
    main()