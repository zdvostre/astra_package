import sys
sys.path.append("..")

from plot_analysis import run_characteristics

def main():
    working_dir = "CHARGE_COMPENSATION\Q_total_3"
    run_number = 2
    
    c = run_characteristics.RunCharacteristics(run_number = run_number, working_dir=working_dir)
    c.save()
    
if __name__ == "__main__":
    main()
    