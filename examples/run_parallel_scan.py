import sys
sys.path.append("..")

import argparse
import configparser
import math

from astra import parallel_scan

working_dir = r"UNIFORM_TOPUP\FULL_OPTIMALISATION_5nC\Q_COMPENSATION"
n_parallel = 6
initial_run_number = None # if None, the run number will be set automatically

def main():
    parser = argparse.ArgumentParser(
        description='Script for performing a parallel scan of given parameter. To run, provide a file with settings.')

    parser.add_argument('--setting_file', type=str, action='store', default="run_parallel_scan.conf",
                        help='Input file with settings for the parallel run (default parallel.conf).')
    parsed = parser.parse_args()
    
    config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
    config.read(parsed.setting_file)
    
    ps = parallel_scan.ParallelScan(working_dir=working_dir,
                                    n_parallel=n_parallel,
                                    )
    
    q_total = [0.5 + q/2 for q in range (12)]
    r = [math.sqrt(q/5)*2.2 for q in q_total]

    ps.add_scanning_parameter("Q_total", values=q_total)
    ps.add_scanning_parameter("Lx", values=r)
        
    ps.log_config_file(parsed.setting_file)
    ps.run_scan()
    ps.plot()
    
if __name__ == '__main__':
    main()