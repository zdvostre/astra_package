# ASTRA package & framework

## Description
This framework is meant to help to run ASTRA programmes for simulations. In this framework, it is possible to perform scans via running the simulations in parallel. There is also a basic plotting module to replace the one in ASTRA.

## Installation
This framework requires [ASTRA](https://www.desy.de/~mpyflo/) programmes. It is best to copy them to each folder, in which the simulations are run. More on this later.

Python 3 is required to use this framework. To download the code, please paste the following lines to the commmand prompt:
```
git clone https://gitlab.cern.ch/zdvostre/astra_package.git
pip install -r requirements.txt
python setup.py
```

## Usage
TODO
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
In case of a problem, feel free to contribute or send an email to the author.

## Contributing
Feel free to contribute.

## Authors and acknowledgment
```
Zdenek Vostrel (zdenek.vostrel@cern.ch)
```